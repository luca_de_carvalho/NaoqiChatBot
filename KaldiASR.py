import numpy as np
from kaldi.asr import NnetLatticeFasterOnlineRecognizer
from kaldi.decoder import LatticeFasterDecoderOptions
from kaldi.nnet3 import NnetSimpleLoopedComputationOptions
from kaldi.online2 import (OnlineEndpointConfig,
                           OnlineIvectorExtractorAdaptationState,
                           OnlineNnetFeaturePipeline,
                           OnlineNnetFeaturePipelineConfig,
                           OnlineNnetFeaturePipelineInfo,
                           OnlineSilenceWeighting)
from kaldi.util.options import ParseOptions
from kaldi.util.table import SequentialWaveReader
from kaldi.matrix import SubVector
import fileinput
import os
import threading
import time
from scipy.io import wavfile


class KaldiASR:
    def __init__(self,chain_path):
        self.chain_path= chain_path
        self.CheckAndPrepConfFile()
        # Define online feature pipeline
        feat_opts = OnlineNnetFeaturePipelineConfig()
        endpoint_opts = OnlineEndpointConfig()
        po = ParseOptions("")
        feat_opts.register(po)
        endpoint_opts.register(po)
        po.read_config_file(chain_path+"conf/online.conf")
       

        self.feat_info = OnlineNnetFeaturePipelineInfo.from_config(feat_opts)

        # Construct recognizer
        decoder_opts = LatticeFasterDecoderOptions()
        decoder_opts.beam = 13
        decoder_opts.max_active = 7000
        self.decodable_opts = NnetSimpleLoopedComputationOptions()
        self.decodable_opts.acoustic_scale = 1.0
        self.decodable_opts.frame_subsampling_factor = 3
        self.decodable_opts.frames_per_chunk = 150
        self.asr = NnetLatticeFasterOnlineRecognizer.from_files(
            chain_path+"final.mdl", chain_path+"graph_pp/HCLG.fst", symbols_filename=chain_path+"graph_pp/words.txt",
            decoder_opts=decoder_opts,
            decodable_opts=self.decodable_opts,
            endpoint_opts=endpoint_opts)



        self.adaptation_state = OnlineIvectorExtractorAdaptationState.from_info(
            self.feat_info.ivector_extractor_info)

        self.feat_pipeline = OnlineNnetFeaturePipeline(self.feat_info)
        #self.feat_pipeline.set_adaptation_state(adaptation_state)
        self.asr.set_input_pipeline(self.feat_pipeline)
        #asr.init_decoding()
        self.sil_weighting = OnlineSilenceWeighting(
            self.asr.transition_model, self.feat_info.silence_weighting_config,
            self.decodable_opts.frame_subsampling_factor)

        
    def CheckAndPrepConfFile(self):
        # this only applies when used with the default model from the instructions 
        # if you chose another model i assumed you set it up yourself so the conf files shoud be correct
        if not self.chain_path.startswith("data/aspire"):
            return 
        onlineConfPath=self.chain_path+"conf/online.conf"
        ivectorConfPath=self.chain_path+"conf/ivector_extractor.conf"
        currentPath= os.path.dirname(os.path.abspath(__file__))+"/data"
        print(self.chain_path)
        print currentPath
        with open(onlineConfPath, 'r') as file :
            filedata = file.read()
        filedata = filedata.replace('PATHTODATA', currentPath)
        with open(onlineConfPath, 'w') as file:
            file.write(filedata)

        with open(ivectorConfPath, 'r') as file :
            filedata = file.read()
        filedata = filedata.replace('PATHTODATA', currentPath)
        with open(ivectorConfPath, 'w') as file:
            file.write(filedata)
        
   
    def decode_wav(self,audio_path,sample_rate=8000):
        """
        wav file decoding for test usage
        """
        _, audio_data = wavfile.read(audio_path)
        audio_data = SubVector(audio_data.astype(np.float32))

        self.feat_pipeline = OnlineNnetFeaturePipeline(self.feat_info)
        self.asr.set_input_pipeline(self.feat_pipeline)
        self.feat_pipeline.accept_waveform(sample_rate, audio_data)
        self.feat_pipeline.input_finished()
        out= self.asr.decode()
        return(out["text"])

    
    def start_decoding_from_audiodata_queue(self,queue,final_callback=None,chunk_size=1440,sample_rate=8000):
        """
        method that starts a thread to run _decode_from_queue in
        """
        self.dataQueue=queue
        self.stream_decoding=True
        threading.Thread(target=self._decode_from_queue,args=[final_callback,chunk_size,sample_rate]).start()
        
    
    def _decode_from_queue(self,final_callback,chunk_size=1440,sample_rate=8000):
        """
        continuous decoding from audio data queue with callback
        """
        self.asr.set_input_pipeline(self.feat_pipeline)
        self.asr.init_decoding()
       
        utt, part = 1, 1
        prev_num_frames_decoded, offset = 0, 0
        last_chunk=False
        i=0
        
        while(self.stream_decoding):
    
            if(self.dataQueue.qsize()<chunk_size):
                time.sleep(.2)
                continue
            
            decoded= [self.dataQueue.get() for _ in range(chunk_size)]
            decoded= np.array(decoded,dtype=np.int16)
           # print(decoded)
            #convert to kaldi subvector 
            
            dataVector=SubVector(decoded.astype("Float32"))
        
            self.feat_pipeline.accept_waveform(sample_rate, dataVector)
            if self.sil_weighting.active():
                self.sil_weighting.compute_current_traceback(self.asr.decoder)
                self.feat_pipeline.ivector_feature().update_frame_weights(
                    self.sil_weighting.get_delta_weights(
                        self.feat_pipeline.num_frames_ready()))
            self.asr.advance_decoding()
            num_frames_decoded = self.asr.decoder.num_frames_decoded()
            if not last_chunk:
                if self.asr.endpoint_detected():
                    self.asr.finalize_decoding()
                    out = self.asr.get_output()
                    if(final_callback is not None):
                        final_callback(out["text"])
                    
                    ##print("final:", out["text"], flush=True)
                    #send_to_placat(out["text"])
                    offset += int(num_frames_decoded
                                    * self.decodable_opts.frame_subsampling_factor
                                    * self.feat_pipeline.frame_shift_in_seconds()
                                    * sample_rate)
                    self.feat_pipeline.get_adaptation_state(self.adaptation_state)
                    self.feat_pipeline = OnlineNnetFeaturePipeline(self.feat_info)
                    self.feat_pipeline.set_adaptation_state(self.adaptation_state)
                    self.asr.set_input_pipeline(self.feat_pipeline)
                    self.asr.init_decoding()
                    #sil_weighting = OnlineSilenceWeighting#(
                    #    asr.transition_model, feat_info.silence_weighting_config,
                    #    decodable_opts.frame_subsampling_factor)
                    remainder = dataVector[offset:i + chunk_size]
                    self.feat_pipeline.accept_waveform(sample_rate, remainder)
                    utt += 1
                    part = 1
                    prev_num_frames_decoded = 0
                elif num_frames_decoded > prev_num_frames_decoded:
                    prev_num_frames_decoded = num_frames_decoded
                    out = self.asr.get_partial_output()
                    #print("-utt%d-part%d" % (utt, part),
                    #        out["text"])
                    part += 1
            i+=1


    
    def _stream_decode(self,audio_stream,final_callback,chunk_size=1440,sample_rate=8000):
        """
        Continuous decoding from pyaudio mic stream with callback
        """
        self.asr.set_input_pipeline(self.feat_pipeline)
        self.asr.init_decoding()
       
        utt, part = 1, 1
        prev_num_frames_decoded, offset = 0, 0
        last_chunk=False
        i=0
        
        while(self.stream_decoding):
            
            dataChunk= audio_stream.read(chunk_size)
        
            decoded=np.frombuffer(dataChunk, dtype=np.int16)
           ## print(decoded)

            #convert to kaldi subvector 
            
            dataVector=SubVector(decoded.astype("Float32"))
        
            self.feat_pipeline.accept_waveform(sample_rate, dataVector)
            if self.sil_weighting.active():
                self.sil_weighting.compute_current_traceback(self.asr.decoder)
                self.feat_pipeline.ivector_feature().update_frame_weights(
                    self.sil_weighting.get_delta_weights(
                        self.feat_pipeline.num_frames_ready()))
            self.asr.advance_decoding()
            num_frames_decoded = self.asr.decoder.num_frames_decoded()
            if not last_chunk:
                if self.asr.endpoint_detected():
                    self.asr.finalize_decoding()
                    out = self.asr.get_output()
                    if(final_callback is not None):
                        final_callback(out["text"])
                    
                    ##print("final:", out["text"], flush=True)
                    #send_to_placat(out["text"])
                    offset += int(num_frames_decoded
                                    * self.decodable_opts.frame_subsampling_factor
                                    * self.feat_pipeline.frame_shift_in_seconds()
                                    * sample_rate)
                    self.feat_pipeline.get_adaptation_state(self.adaptation_state)
                    self.feat_pipeline = OnlineNnetFeaturePipeline(self.feat_info)
                    self.feat_pipeline.set_adaptation_state(self.adaptation_state)
                    self.asr.set_input_pipeline(self.feat_pipeline)
                    self.asr.init_decoding()
                    #sil_weighting = OnlineSilenceWeighting#(
                    #    asr.transition_model, feat_info.silence_weighting_config,
                    #    decodable_opts.frame_subsampling_factor)
                    remainder = dataVector[offset:i + chunk_size]
                    self.feat_pipeline.accept_waveform(sample_rate, remainder)
                    utt += 1
                    part = 1
                    prev_num_frames_decoded = 0
                elif num_frames_decoded > prev_num_frames_decoded:
                    prev_num_frames_decoded = num_frames_decoded
                    out = self.asr.get_partial_output()
                    print("-utt%d-part%d" % (utt, part),
                            out["text"])
                    part += 1
            i+=1

    
    def start_stream_decode(self,audio_stream,final_callback=None,chunk_size=1440,sample_rate=8000):
        """
        method that starts a thread to run _stream_decode in
        """
        self.stream_decoding=True
        threading.Thread(target=self._stream_decode,args=[audio_stream,final_callback,chunk_size,sample_rate]).start()
        
    def stop_stream_decode(self):
        self.stream_decoding=False

