from KaldiASR import KaldiASR
import pyaudio
import requests
import time
import json
#from TTSModule import  TTS

#=====================================
#PYAUDIO SETUP
#=================================




class LocalAudioBot:

    def __init__(self):
        #self.tts=TTS()
        #self.say("hi, how are you?")
        self.kaldi_asr= KaldiASR("../../aspire/s5/exp/tdnn_7b_chain_online/")

    def send_to_placat(self,question):
        print("sending")
        
        print(question)
        url = 'https://8c223f4e5459.ngrok.io'
        headers = { 'Content-Type': 'application/json' }
        payload = { 
            'session': 'projects/your-agents-project-id/agent/sessions/88d13aa8-2999-4f71-b233-39cbf3a824a0',
            'queryResult': { 'queryText': question } }

        r = requests.post(url, data=json.dumps(payload), headers=headers)
        print(r)
        answer = r.json()['fulfillmentText']
        
        if not answer:
            answer = 'No answer'
        print(answer)

    def processSentence(self,sentence):
        print(sentence)
        words= sentence.split(" ")
       # if "[noise]" not in words and "<unk>" not in words and sentence != 'mhm' and sentence.strip() !="":
       #     self.send_to_placat(sentence)
            
    def say(self,text):
        #self.tts.say(text)
        print(text)

    def startWithLocalAudio(self):
        CHUNK_SIZE = 1440
        FORMAT = pyaudio.paInt16
        CHANNELS = 1
        RATE = 8000
        RECORD_SECONDS = 180


        p = pyaudio.PyAudio()
        ##self.kaldi_asr= KaldiASR("../../kaldi/egs/aspire/s5/exp/tdnn_7b_chain_online/")
        #kaldi_asr= KaldiASR("../../0013_librispeech_v1/exp/chain_cleaned/tdnn_1d_sp")

        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK_SIZE)

        
        self.kaldi_asr.start_stream_decode(stream,self.processSentence)
        
    def stop(self):
        self.kaldi_asr.stop_stream_decode()
    def printSentence(self,sentence):
        print(sentence)
            
def main():
    
    bot= LocalAudioBot()
    bot.startWithLocalAudio()
    time.sleep(100)
    bot.stop()



if __name__ == "__main__":
    # execute only if run as a script
    main() 