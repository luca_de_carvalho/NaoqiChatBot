import torch
import sys
import time
sys.path.append("data/TTSResources/espnet/egs/ljspeech/tts1/local")
sys.path.append("data/TTSResources/espnet")
from argparse import Namespace
from espnet.asr.asr_utils import get_model_conf
from espnet.asr.asr_utils import torch_load
from espnet.utils.dynamic_import import dynamic_import
import yaml
import parallel_wavegan.models
import pyaudio
import numpy as np
from text.cleaners import custom_english_cleaners
from g2p_en import G2p

trans_type = "phn"
dict_path = "data/TTSResources/en/tacotron2/data/lang_1phn/phn_train_no_dev_units.txt"
model_path = "data/TTSResources/en/tacotron2/exp/phn_train_no_dev_pytorch_train_pytorch_tacotron2.v4/results/model.last1.avg.best"

vocoder_path = "data/TTSResources/en/parallel_wavegan/ljspeech.parallel_wavegan.v2/checkpoint-400000steps.pkl"
vocoder_conf = "data/TTSResources/en/parallel_wavegan/ljspeech.parallel_wavegan.v2/config.yml"


class TTS:
    def __init__(self):

        self.device = torch.device("cuda")

        # define E2E-TTS model
        
        self.idim, self.odim, train_args = get_model_conf(model_path)
        model_class = dynamic_import(train_args.model_module)
        self.model = model_class(self.idim, self.odim, train_args)
        torch_load(model_path, self.model)
        self.model = self.model.eval().to(self.device)
        self.inference_args = Namespace(**{
            "threshold": 0.5,"minlenratio": 0.0, "maxlenratio": 10.0,
            "use_attention_constraint": True,
            "backward_window": 1, "forward_window":3,
            })

        # define neural vocoder
      
        with open(vocoder_conf) as f:
            self.config = yaml.load(f, Loader=yaml.Loader)
        self.vocoder_class = self.config.get("generator_type", "ParallelWaveGANGenerator")
        self.vocoder = getattr(parallel_wavegan.models, self.vocoder_class)(**self.config["generator_params"])
        self.vocoder.load_state_dict(torch.load(vocoder_path, map_location="cpu")["model"]["generator"])
        self.vocoder.remove_weight_norm()
        self.vocoder = self.vocoder.eval().to(self.device)

        with open(dict_path) as f:
            lines = f.readlines()
        lines = [line.replace("\n", "").split(" ") for line in lines]
        self.char_to_id = {c: int(i) for c, i in lines}
        self.g2p = G2p()
    

    def _frontend(self,text):
        """Clean text and then convert to id sequence."""
        text = custom_english_cleaners(text)
        
        if trans_type == "phn":
            text = filter(lambda s: s != " ", self.g2p(text))
            text = " ".join(text)
            ##print("Cleaned text: {text}")
            charseq = text.split(" ")
        else:
            ##print("Cleaned text: {text}")
            charseq = list(text)
        idseq = []
        for c in charseq:
            if c.isspace():
                idseq += [self.char_to_id["<space>"]]
            elif c not in self.char_to_id.keys():
                idseq += [self.char_to_id["<unk>"]]
            else:
                idseq += [self.char_to_id[c]]
        idseq += [self.idim - 1]  # <eos>
        return torch.LongTensor(idseq).view(-1).to(self.device)



    def say(self,text):
        pad_fn = torch.nn.ReplicationPad1d(
        self.config["generator_params"].get("aux_context_window", 0))
        use_noise_input = self.vocoder_class == "ParallelWaveGANGenerator"
        with torch.no_grad():
            start = time.time()
            x = self._frontend(text)
            c, _, _ = self.model.inference(x, self.inference_args)
            c = pad_fn(c.unsqueeze(0).transpose(2, 1)).to(self.device)
            xx = (c,)
            if use_noise_input:
                z_size = (1, 1, (c.size(2) - sum(pad_fn.padding)) * self.config["hop_size"])
                z = torch.randn(z_size).to(self.device)
                xx = (z,) + xx
            y = self.vocoder(*xx).view(-1)
        rtf = (time.time() - start) / (len(y) / self.config["sampling_rate"])
        ##print(f"RTF = {rtf:5f}")

        samples=y.view(-1).cpu().numpy()

     


        p = pyaudio.PyAudio()
        stream = p.open(format=pyaudio.paFloat32,
                         channels=1,
                         rate=self.config["sampling_rate"],
                         output=True
                         )
        # Assuming you have a numpy array called samples
        data = samples.astype(np.float32).tostring()
        stream.write(data)