from NaoControl import NaoControlModule
import argparse
from KaldiASR import KaldiASR
import argparse
import sys
import qi
import time
import io
import pyaudio
import Queue
import signal
import requests
import json
import numpy as np
#from TTSModule import  TTS

class NaoqiChatBot:

    def __init__(self,robotIP,robotPort,face_needed=False,face_tracking=False, local_audio=False):
        with open("config.json") as json_data_file:
            config_data = json.load(json_data_file)
        modelPath=str(config_data["KaldiConfig"]["modelPath"])
        placatIP=config_data["PlacatConfig"]["ip"]
        placatPort=config_data["PlacatConfig"]["port"]
        self.placatUrl='http://'+placatIP+':'+str(placatPort)+'/'

        self.kaldi_asr= KaldiASR(modelPath)
       
        self.localAudio=local_audio
        if (not local_audio):

            self.audioDataQueue= Queue.Queue()

            print ("connecting to Robot on "+robotIP+":"+str(robotPort))
            try:
                # Initialize qi framework.
                connection_url = "tcp://" + robotIP + ":" + str(robotPort)
                self.app = qi.Application(["NaoControlModule", "--qi-url=" + connection_url])
            except RuntimeError:
                print ("Can't connect to Naoqi at ip \"" + robotIP+ "\" on port " + str(robotPort) +".\n"
                    "Please check your script arguments. Run with -h option for help.")
                sys.exit(1)
            self.MyControlModule = NaoControlModule(self.app,self.add_to_audio_data,face_needed=face_needed,tracking_active=face_tracking)

            self.app.session.registerService("NaoControlModule", self.MyControlModule)
            
            ##self.audioData=[]
        ##else:
            ##self.tts=TTS()
           
            


      
        

    def send_to_placat(self,question):
        """
        sends the question to placat and then sends its answer (or "i dont know" if there isn't any) to the robot's text to speech
        """
        print("sending:<"+question+"> to PLACAT")    
        headers = { 'Content-Type': 'application/json' }
        payload = { 
            'session': 'projects/your-agents-project-id/agent/sessions/88d13aa8-2999-4f71-b233-39cbf3a824a0',
            'queryResult': { 'queryText': question } }

        r = requests.post(self.placatUrl, data=json.dumps(payload), headers=headers)
        
        answer = r.json()['fulfillmentText']
        
        print(answer)
        if not answer:
            self.say("I don't know")
        else:
            self.say(answer)


   
    def processSentence(self,sentence):
        """
        Callback function for complete speech utterance detected by the ASR that
        processes the sentence and filters out common tags given by the ASR and sends the sentence to kaldi if it isn't empty 
        (or if it matches a list of common false positives often "detected" from the noise picked up by the mic)
        """
        common_false_positives=[["yeah"],["mhm"],["uh-huh"]]
        print(sentence)
        filteredWords=["[noise]","[laughter]"]
        words= sentence.split()
        resultwords  = [word for word in words if word not in filteredWords ]
        result = ' '.join(resultwords)
        if("<unk>" in resultwords):
            self.say("I didn't quite get that sorry")
            return
        if(resultwords != [] and resultwords not in common_false_positives ):
            self.send_to_placat(result)

    def say(self,text):
        """ 
        vocalizes the answer either through the robot or by using the tts module
        """
        if( not self.localAudio):
            self.MyControlModule.say(text)
        else:
            print(answer)
            ##self.tts.say(text)


    def startWithLocalAudio(self):
        """
        starts listening on local microphone
        """
        CHUNK_SIZE = 1440
        FORMAT = pyaudio.paInt16
        CHANNELS = 1
        RATE = 8000
     
        p = pyaudio.PyAudio()
       
        self.stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK_SIZE)

        
        self.kaldi_asr.start_stream_decode(self.stream,self.processSentence)
    
    
    def add_to_audio_data(self,newData):
        """
        callback for the audio processing
        that puts the audio data in the queue that the ASR consumes
        """
        ##self.audioData.append(newData)
        map(self.audioDataQueue.put, newData)
       


    
    def start_listening(self):
        """
        start the audio processing and live decoding 
        """
        if( not self.localAudio):
            self.MyControlModule.set_audio_processing_callback(self.add_to_audio_data)
            self.MyControlModule.startProcessing()
            self.kaldi_asr.start_decoding_from_audiodata_queue(self.audioDataQueue,self.processSentence)
            
        else:
            self.startWithLocalAudio()
        
        self.say("Hi, ask me anything!")
   
    def stop_listening(self):
        """
        shuts the audio processing and live decoding down 
        """
        self.kaldi_asr.stop_stream_decode()
        if( not self.localAudio):
            self.MyControlModule.stopProcessing()
        else:
            self.stream.close()
   
   
    def playaudio(self):
        """
        Test method to playback the recorded audio 
        """
       # samples=np.array(self.audioData)
        p = pyaudio.PyAudio()
        stream = p.open(format=pyaudio.paFloat32,
                         channels=1,
                         rate=8000,
                         output=True
                         )
        
        data = samples.astype(np.float32).tostring()
        stream.write(data)
        
    

    def interrupt_handler(self,x,y):
        """
        Callback for the interupt signal(ctrl+c) to ensure proper shutdown of all components
        """
        self.stop_listening()
        sys.exit(0)


def main():
    
  
    parser = argparse.ArgumentParser()

    parser.add_argument('-ip',dest='ip',help="the robots IP adress")
    parser.add_argument('-port', dest='port',default=9559)
    
    add_bool_arg(parser,'face_required')
    add_bool_arg(parser,'face_tracking')
    add_bool_arg(parser,'local_audio')
    
 
    args = parser.parse_args()
    port= args.port
    ip=args.ip
    localAudio= args.local_audio
    if(ip is None):
        if(not localAudio):
            parser.print_help()
            sys.exit(2)

    face_req=args.face_required
    face_track=args.face_tracking

    bot= NaoqiChatBot(ip,port,face_needed=face_req,face_tracking=face_track,local_audio=localAudio)
    signal.signal(signal.SIGINT,bot.interrupt_handler)
   
    bot.start_listening()

    while True:
        print "Press (Ctrl+C) to shutdown "
        signal.pause()
 
def add_bool_arg(parser, name, default=False):
    group = parser.add_mutually_exclusive_group(required=False)
    group.add_argument('--' + name, dest=name, action='store_true')
    group.add_argument('--no-' + name, dest=name, action='store_false')
    parser.set_defaults(**{name:default})

if __name__ == "__main__":
    main()