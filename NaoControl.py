#! /usr/bin/env python
# -*- encoding: UTF-8 -*-


import qi
import argparse
import sys
import time
import numpy as np
import threading

class NaoControlModule(object):
    """
    Module that controls the robot
    """

    def __init__( self, app, audio_callback=None, face_needed=True,tracking_active=False):
        """
        Initialize services and variables.
        """
        super(NaoControlModule, self).__init__()
        app.start()
        session = app.session

        
        self.tracking_active=tracking_active
        self.face_needed=face_needed    
            
            
        # Get the services necessary to get the mic input and text-to-speech working
        self.audio_process_callback=audio_callback
        self.audio_service = session.service("ALAudioDevice")

        self.memory= session.service("ALMemory")
        self.TTS_service= session.service("ALTextToSpeech")
        self.TTS_service.setLanguage("English")
        self.TTSEndSubscriber = self.memory.subscriber("ALTextToSpeech/TextDone")
        self.TTSEndSubscriber.signal.connect(self.on_tts_done)        

        self.module_name = "NaoControlModule"
        self.processing=False
        
        #if tracking is active we wake the robot up, get the tracker services and start tracking 
        if(self.tracking_active):
            self.motion_service = session.service("ALMotion")
            self.tracker_service = session.service("ALTracker")
            self.motion_service.wakeUp()
            self.targetName = "Face"
            self.faceWidth = 0.1
            self.tracker_service.registerTarget(self.targetName, self.faceWidth)
            # Then, start tracker.
            self.tracker_service.track(self.targetName)
        
        #if a face is needed for the robot to listen, subscribe to the necessary events
        if(self.face_needed):
            self.faceDetectionSubscriber = self.memory.subscriber("FaceDetected")
            self.faceDetectionSubscriber.signal.connect(self.on_human_tracked)
            self.face_detection = session.service("ALFaceDetection")
            self.face_detection.subscribe("HumanGreeter")
            self.got_face = False

   
        



    def on_human_tracked(self, value):
        """
        Callback for event FaceDetected.
        """
        if value == []:  # empty value when the face disappears
            self.got_face = False
            self.say("Goodbye")
            #self.pauseProcessing()
            print "no Face"
        elif not self.got_face:  # only speak the first time a face appears
            self.got_face = True
            
            print "I saw a face!"
            self.say("I'm listening")
            #self.resumeProcessing();


    def set_audio_processing_callback(self,callback):
        self.audio_process_callback=callback


    def startProcessing(self):
        """
        Start processing 
        """
        # ask for the front microphone signal sampled at 16kHz
        # if you want the 4 channels call setClientPreferences(self.module_name, 48000, 0, 0)
        self.audio_service.setClientPreferences(self.module_name, 16000, 3, 0)
        self.audio_service.subscribe(self.module_name)
        self.processing=True


    def processRemote(self, nbOfChannels, nbOfSamplesByChannel, timeStamp, inputBuffer):
        
        ## interrupts the processing if there are no faces in the field of view 
        if(self.face_needed and not self.got_face):
            return
        
        
        ##sends the audio from the robots mic to the callback function  if processing is true and a callback is set
        if( self.processing and self.audio_process_callback is not None):
            self.audio_process_callback(self.convertStr2SignedInt8khz(inputBuffer))



    
    def on_tts_done(self,b): 
        """
        Callback for TextDone event to resume audio once the text-to-speech is over
        """
        if b and not self.processing:
            print "tts done"
            self.resumeProcessing()

    def say(self, sentence):
        """
        Pauses audio processing before using the text-to-speech to avoid hearing itself
        """
        self.pauseProcessing()
        self.TTS_service.say(sentence)
 
    def pauseProcessing(self):
        print "pause"
        self.processing=False
    
    def resumeProcessing(self):
        print "resume"
        self.processing=True

    def stopProcessing(self):
        """
        unsubscribes from the audioservice and stops tracking if it is active
        """
        self.audio_service.unsubscribe(self.module_name)
        if(self.tracking_active):
            self.stopTracking()
   

    def convertStr2SignedInt8khz(self, data) :
        """
        This function takes a string containing 16 bits little endian sound
        samples as input and returns a vector containing the 16 bits values.
        it also converts the 16khz input from the mic to the 8hz needed by the aspire model by skipping every other sample.
        """
        signedData=[]
        ind=0
        for i in range (0,len(data)/4) :
            signedData.append(data[ind]+data[ind+1]*256)
            ind=ind+4

       
        return signedData

    def stopTracking(self):
        """
        Shuts the tracking service down and sets the robot to rest
        """
        self.tracker_service.stopTracker()
        self.tracker_service.unregisterAllTargets()
        self.motion_service.rest()
   


