# NaoqiChatBot

Chatbot Vocal pour robots NaoQi (Pepper, Nao) basé sur [PLACAT](https://github.com/heig-iict-ida/PLACAT)


## Installation

Ce système est basé sur le chatbot [PLACAT](https://github.com/heig-iict-ida/PLACAT) et nécessite son installation pour fonctionner.


-Creer un environnement conda et y pip install les requirements
-installer pykaldi avec: conda install -c pykaldi pykaldi
-installer la version compatible avec votre machine de pytorch (pytorch.org)

-télécharger les modèles nécessaires :[Data](https://drive.google.com/file/d/1VmAyPdXKB5DjK6FWKnaeu2BafoEvxbTk/view?usp=sharing) et extraire à la racine du repo


### Utilisation

-Démarrer PLACAT
-Récuperer l'adresse IP du robot
-executer python NaoqiChatBot.py -ip <IPDUROBOT>
-les arguments --face_tracking et --face_required peuvent etre ajouté

